package com.example.valorantapi

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageButton
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.example.valorantapi.databinding.ActivityMainBinding
import com.example.valorantapi.databinding.ListScreenBinding

class MenuFragment: Fragment(R.layout.list_screen) {

    //private lateinit var binding: ListScreenBinding

    private lateinit var agentsButton: ImageButton
    private lateinit var comptetitiveTiersButton: ImageButton
    private lateinit var mapsButton: ImageButton
    private lateinit var weaponsButton: ImageButton
    private lateinit var favsButton: Button

    private lateinit var viewModel: ValorantApiViewModel

/*
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = ListScreenBinding.inflate(layoutInflater)
        return binding.root
    }
*/

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        agentsButton = view.findViewById(R.id.agentsImage)
        //comptetitiveTiersButton = view.findViewById(R.id.competitiveTiersImage)
        mapsButton = view.findViewById(R.id.mapsImage)
        weaponsButton = view.findViewById(R.id.weaponsImage)
        favsButton = view.findViewById(R.id.favs_button)

        viewModel = ViewModelProvider(this).get(ValorantApiViewModel::class.java)

        agentsButton.setOnClickListener{
            findNavController().navigate(R.id.action_menuFragment_to_agentsFragment)
        }
        //comptetitiveTiersButton.setOnClickListener{
        //    findNavController().navigate(R.id.action_menuFragment_to_competitiveTiersFragment)
        //}
        mapsButton.setOnClickListener{
            findNavController().navigate(R.id.action_menuFragment_to_mapsFragment)
        }
        weaponsButton.setOnClickListener{
            findNavController().navigate(R.id.action_menuFragment_to_weaponsFragment)
        }

        favsButton.setOnClickListener {
            findNavController().navigate(R.id.action_menuFragment_to_favouriteListFragment)
        }
    }
}