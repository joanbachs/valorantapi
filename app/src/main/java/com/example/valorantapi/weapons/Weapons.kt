package com.example.valorantapi.weapons

data class Weapons(
    val data: List<DataWeapon>,
    val status: Int
)