package com.example.valorantapi.weapons

data class GridPosition(
    val column: Int,
    val row: Int
)