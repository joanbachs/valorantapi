package com.example.valorantapi.weapons

data class DamageRange(
    val bodyDamage: Int,
    val headDamage: Float,
    val legDamage: Double,
    val rangeEndMeters: Int,
    val rangeStartMeters: Int
)