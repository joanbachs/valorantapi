package com.example.valorantapi.agents

data class Media(
    val id: Int,
    val wave: String,
    val wwise: String
)