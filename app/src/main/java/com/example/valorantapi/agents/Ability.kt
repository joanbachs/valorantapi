package com.example.valorantapi.agents

data class Ability(
    val description: String,
    val displayIcon: String,
    val displayName: String,
    val slot: String
)