package com.example.valorantapi.agents

data class Data(
    val data: List<AgentData>,
    val status: Int
)