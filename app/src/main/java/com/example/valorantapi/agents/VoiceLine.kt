package com.example.valorantapi.agents

import com.example.valorantapi.agents.Media

data class VoiceLine(
    val maxDuration: Double,
    val mediaList: List<Media>,
    val minDuration: Double
)