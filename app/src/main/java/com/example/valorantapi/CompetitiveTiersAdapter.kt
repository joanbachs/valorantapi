package com.example.valorantapi

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.valorantapi.agents.AgentData
import com.example.valorantapi.competitiveTiers.Tier
import com.squareup.picasso.Picasso

class CompetitiveTiersAdapter(ct: List<Tier>): RecyclerView.Adapter<CompetitiveTiersAdapter.ValorantApiViewHolder>() {
    // L'adapter agafarà la llista d'items que volem pinter i s'encarregarà de
    // gestionar tota interacció amb ella
    private val CTs = ct

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CompetitiveTiersAdapter.ValorantApiViewHolder {
        // Crea un layout recyclerview_item.xml i li passa a la classe ViewHolder
        val v = LayoutInflater
            .from(parent.context)
            .inflate(R.layout.recyclerview_item, parent, false)
        return ValorantApiViewHolder(v)
    }

    override fun onBindViewHolder(holder: CompetitiveTiersAdapter.ValorantApiViewHolder, position: Int) {
        // Cridarà al ViewHolder perquè pinti la informació d'un item
        holder.bindCtData(CTs[position])
    }

    // Tindrà el número d'elements de la llista de l'adapter
    override fun getItemCount(): Int = CTs.size

    class ValorantApiViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
        private var snapImageView: ImageView = itemView.findViewById(R.id.snap_imageview)
        private var nameTextView: TextView = itemView.findViewById(R.id.name_textview)
        private var tierTextView: TextView = itemView.findViewById(R.id.role_textview)

        fun bindCtData(ct: Tier) {
            if (ct != null){
                Picasso.get().load(ct.largeIcon).into(snapImageView)
                nameTextView.text = ct.tierName
                tierTextView.text = ct.tier.toString()
            }
        }
    }
}
