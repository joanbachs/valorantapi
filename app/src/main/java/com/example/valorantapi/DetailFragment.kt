package com.example.valorantapi

import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.example.valorantapi.Entities.AgentEntity
import com.example.valorantapi.Entities.ValorantApplication
import com.example.valorantapi.agents.AgentData
import com.example.valorantapi.agents.Data
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

private lateinit var image: ImageView
private lateinit var agentName: TextView
private lateinit var description: TextView
private lateinit var favButton: Button
private lateinit var viewModel: ValorantApiViewModel


class DetailFragment: Fragment(R.layout.detail_screen) {
    //private lateinit var binding: FragmentAddContactBinding

    private lateinit var adapter: WeaponAdapter
    private var layoutType = 0
    //private val viewModel: ValorantApiViewModel by activityViewModels()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel = ViewModelProvider(this).get(ValorantApiViewModel::class.java)

        image = view.findViewById(R.id.icon)
        agentName = view.findViewById(R.id.itemName)
        description = view.findViewById(R.id.description)

        Glide.with(image.context)
            .load(arguments?.getString("image"))
            .diskCacheStrategy(DiskCacheStrategy.ALL)
            .fitCenter()
            .into(image)
        agentName.text = arguments?.getString("name")
        description.text = arguments?.getString("description")

        val textAgentName = agentName.text.toString()
        val textAgentImage = image.toString()
        val textAgentDescription = description.text.toString()

        favButton = view.findViewById(R.id.favButton)
        favButton.setOnClickListener {
            val newFav = AgentEntity(name = textAgentName, image = textAgentImage, description = textAgentDescription, favourite = true)
            insertFav(newFav)
        }
    }

    private fun insertFav(agent: AgentEntity) {
        CoroutineScope(Dispatchers.IO).launch {
            ValorantApplication.database.agentDao().addFavourite(agent)
            println(ValorantApplication.database.agentDao().getFavoriteAgents())
        }
    }

    private fun deleteFav(agent: AgentEntity){
        CoroutineScope(Dispatchers.IO).launch {
            ValorantApplication.database.agentDao().deleteFavourite(agent)
            println(ValorantApplication.database.agentDao().getFavoriteAgents())
        }
    }
}