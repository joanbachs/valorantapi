package com.example.valorantapi

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.valorantapi.Entities.AgentEntity
import com.example.valorantapi.Entities.ValorantApplication
import com.example.valorantapi.agents.AgentData
import com.example.valorantapi.agents.Data
import com.example.valorantapi.competitiveTiers.DataCT
import com.example.valorantapi.maps.DataMaps
import com.example.valorantapi.maps.Maps
import com.example.valorantapi.weapons.DataWeapon
import com.example.valorantapi.weapons.Weapons
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class ValorantApiViewModel: ViewModel() {
    val repository = Repository()
    var agentsData = MutableLiveData<Data>()
    var ctData = MutableLiveData<DataCT>()
    var mapsData = MutableLiveData<Maps>()
    var weaponData = MutableLiveData<Weapons>()

    var agentsFavourite = MutableLiveData<MutableList<AgentEntity>>()

    init {
        fetchAgentData("v1/agents")
        fetchCTData("v1/competitivetiers")
        fetchMapsData("v1/maps")
        fetchWeaponData("v1/weapons")
        fetchAgentData("favs")
    }


    fun fetchAgentData(url: String){
        if (url.equals("favs"))
        CoroutineScope(Dispatchers.IO).launch {
            val favs = ValorantApplication.database.agentDao().getFavoriteAgents()
            agentsFavourite.postValue(favs)

            println(ValorantApplication.database.agentDao().getFavoriteAgents())
        }else{
            CoroutineScope(Dispatchers.IO).launch {
                val response = repository.getAgentsData(url)
                withContext(Dispatchers.Main) {
                    if(response.isSuccessful){
                        agentsData.postValue(response.body())
                    }
                    else{
                        Log.e("Error :", response.message())
                    }
                }
            }
        }
    }

    fun fetchCTData(url: String){
        CoroutineScope(Dispatchers.IO).launch {
            val response = repository.getCTData(url)
            withContext(Dispatchers.Main) {
                if(response.isSuccessful){
                    ctData.postValue(response.body())
                }
                else{
                    Log.e("Error :", response.message())
                }
            }
        }
    }

    fun fetchMapsData(url: String){
        CoroutineScope(Dispatchers.IO).launch {
            val response = repository.getMapsData(url)
            withContext(Dispatchers.Main) {
                if(response.isSuccessful){
                    mapsData.postValue(response.body())
                }
                else{
                    Log.e("Error :", response.message())
                }
            }
        }
    }

    fun fetchWeaponData(url: String){
        CoroutineScope(Dispatchers.IO).launch {
            val response = repository.getWeaponsData(url)
            withContext(Dispatchers.Main) {
                if(response.isSuccessful){
                    weaponData.postValue(response.body())
                }
                else{
                    Log.e("Error :", response.message())
                }
            }
        }
    }
    /*
    fun fetchCTData(url: String){
        ctData = repository.getCTData(url)
    }
    fun fetchMapsData(url: String){
        mapsData = repository.getMapsData(url)
    }
    fun fetchWeaponData(url: String){
        weaponData = repository.getWeaponsData(url)
    }
    */

    /*
    val call = apiInterface.getMapsData()
    call.enqueue(object: Callback<Data> {
        override fun onFailure(call: Call<Data>, t: Throwable) {
            Log.e("ERROR", t.message.toString())
        }
        override fun onResponse(call: Call<Data?>, response: Response<Data?>) {
            if (response != null && response.isSuccessful) {
                val myData = response.body()
            }
        }
    })
    */
}