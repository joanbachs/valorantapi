package com.example.valorantapi

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.valorantapi.competitiveTiers.DataCT

private lateinit var recyclerView: RecyclerView
private lateinit var viewModel: ValorantApiViewModel

class CompetitiveTiersFragment: Fragment(R.layout.competitive_tiers_list_screen) {
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel = ViewModelProvider(this).get(ValorantApiViewModel::class.java)

        recyclerView = view.findViewById(R.id.recyclerview)

        viewModel.ctData.observe(viewLifecycleOwner, Observer {
            setupRecyclerView(it)
        })
    }

    fun setupRecyclerView(ctData: DataCT) {
            recyclerView.layoutManager = LinearLayoutManager(context)
            recyclerView.adapter = CompetitiveTiersAdapter(ctData.tiers)

    }
}