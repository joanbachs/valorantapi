package com.example.valorantapi.maps

data class DataMaps(
    val assetPath: String,
    val callouts: List<Callout>,
    val coordinates: String,
    val displayIcon: String,
    val displayName: String,
    val listViewIcon: String,
    val mapUrl: String,
    val splash: String,
    val uuid: String,
    val xMultiplier: String,
    val xScalarToAdd: Double,
    val yMultiplier: String,
    val yScalarToAdd: Double
)