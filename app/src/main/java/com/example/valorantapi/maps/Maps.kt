package com.example.valorantapi.maps

data class Maps(
    val data: List<DataMaps>,
    val status: Int
)