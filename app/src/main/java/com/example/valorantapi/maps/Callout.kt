package com.example.valorantapi.maps

data class Callout(
    val location: Location,
    val regionName: String,
    val superRegionName: String
)