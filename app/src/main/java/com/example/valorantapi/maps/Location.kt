package com.example.valorantapi.maps

data class Location(
    val x: Double,
    val y: Double
)