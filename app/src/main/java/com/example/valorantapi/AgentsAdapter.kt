package com.example.valorantapi

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.valorantapi.Entities.AgentEntity
import com.example.valorantapi.agents.AgentData
import com.example.valorantapi.databinding.RecyclerviewItemBinding
import com.squareup.picasso.Picasso

class AgentsAdapter(agent: List<AgentData>, private val listener: OnClickListener): RecyclerView.Adapter<AgentsAdapter.ValorantApiViewHolder>() {
    // L'adapter agafarà la llista d'items que volem pinter i s'encarregarà de
    // gestionar tota interacció amb ella
    private val agents = agent

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AgentsAdapter.ValorantApiViewHolder {
        // Crea un layout recyclerview_item.xml i li passa a la classe ViewHolder
        val v = LayoutInflater
            .from(parent.context)
            .inflate(R.layout.recyclerview_item, parent, false)
        return ValorantApiViewHolder(v)
    }

    override fun onBindViewHolder(holder: AgentsAdapter.ValorantApiViewHolder, position: Int) {
        // Cridarà al ViewHolder perquè pinti la informació d'un item
        holder.bindAgentsData(agents[position])
    }

    // Tindrà el número d'elements de la llista de l'adapter
    override fun getItemCount(): Int = agents.size

    inner class ValorantApiViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
        val binding = RecyclerviewItemBinding.bind(itemView)

        fun setListener(agentData: AgentData){
            binding.root.setOnClickListener {
                listener.onClickAgent(agentData)
            }
        }

        fun bindAgentsData(agent: AgentData) {
            if (agent.isPlayableCharacter){
                setListener(agent)
                binding.nameTextview.text = agent.displayName
                binding.roleTextview.text = agent.role.displayName
                Picasso.get().load(agent.displayIcon).into(binding.snapImageview)
            }
        }
    }
}
