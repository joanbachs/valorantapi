package com.example.valorantapi.competitiveTiers

data class Tier(
    val backgroundColor: String,
    val color: String,
    val division: String,
    val divisionName: String,
    val largeIcon: String,
    val rankTriangleDownIcon: Any,
    val rankTriangleUpIcon: Any,
    val smallIcon: String,
    val tier: Int,
    val tierName: String
)