package com.example.valorantapi.competitiveTiers

data class CompetitiveTiers(
    val data: List<DataCT>,
    val status: Int
)