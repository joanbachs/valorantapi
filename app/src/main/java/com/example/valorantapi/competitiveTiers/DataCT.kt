package com.example.valorantapi.competitiveTiers

data class DataCT(
    val assetObjectName: String,
    val assetPath: String,
    val tiers: List<Tier>,
    val uuid: String
)