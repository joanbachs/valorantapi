package com.example.valorantapi.Entities

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.valorantapi.R
import com.example.valorantapi.agents.AgentData
import com.example.valorantapi.databinding.FavsScreenBinding
import com.example.valorantapi.databinding.RecyclerviewItemBinding
import com.squareup.picasso.Picasso

class AdapterRoom(private var agents: MutableList<AgentEntity>, private var listener: OnClickListenerRoom): RecyclerView.Adapter<AdapterRoom.ViewHolder>() {

    private lateinit var context: Context
    private lateinit var recyclerView: RecyclerView

    inner class ViewHolder(view: View): RecyclerView.ViewHolder(view) {
        val binding = RecyclerviewItemBinding.bind(view)

        fun setListener(contact: AgentEntity){
            binding.root.setOnClickListener {
                listener.onClick(contact)
            }
        }

        fun bindFavs(agent: AgentEntity) {
            binding.nameTextview.text = agent.name
            binding.roleTextview.text = agent.description
            Picasso.get().load(agent.image).into(binding.snapImageview)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        context = parent.context
        val view = LayoutInflater.from(context).inflate(R.layout.recyclerview_item, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val agent = agents[position]
        with(holder){
            setListener(agent)
        }
        holder.bindFavs(agent)
    }

    override fun getItemCount(): Int {
        return agents.size
    }

    fun setAgentsList(agentsList: MutableList<AgentEntity>){
        this.agents = agentsList
        notifyDataSetChanged()
    }

}