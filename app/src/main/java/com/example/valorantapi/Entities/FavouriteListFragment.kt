package com.example.valorantapi.Entities

import android.os.Bundle
import android.view.*
import androidx.fragment.app.Fragment
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.valorantapi.R
import com.example.valorantapi.ValorantApiViewModel
import com.example.valorantapi.databinding.FavsScreenBinding
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext


private lateinit var recyclerView: RecyclerView
private lateinit var viewModel: ValorantApiViewModel

class FavouriteListFragment : Fragment(), OnClickListenerRoom {
    private lateinit var adapterRoom: AdapterRoom
    private lateinit var mLayoutManager: RecyclerView
    private lateinit var binding: FavsScreenBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FavsScreenBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        recyclerView = view.findViewById(R.id.favs_recyclerview)
        viewModel = ViewModelProvider(this).get(ValorantApiViewModel::class.java)
        viewModel.agentsFavourite.observe(viewLifecycleOwner, Observer { setupRecyclerView(it) })
    }

    private fun setupRecyclerView(fav: MutableList<AgentEntity>) {
        adapterRoom = AdapterRoom(mutableListOf(), this)
        //mLayoutManager = LinearLayoutManager(context)
        recyclerView.layoutManager = LinearLayoutManager(context)
        recyclerView.adapter = AdapterRoom(fav, this)
        CoroutineScope(Dispatchers.Main).launch {
            val contactList = withContext(Dispatchers.IO) {
                ValorantApplication.database.agentDao().getFavoriteAgents()
            }
            adapterRoom.setAgentsList(contactList)
        }
    }

    override fun onClick(agent: AgentEntity) {
        TODO("Not yet implemented")
    }

    override fun onClickDelete(agent: AgentEntity) {
        TODO("Not yet implemented")
    }

    override fun onClickFavorite(agent: AgentEntity) {
        TODO("Not yet implemented")
    }


}