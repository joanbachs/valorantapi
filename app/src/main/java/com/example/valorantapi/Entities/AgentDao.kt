package com.example.valorantapi.Entities

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Query

@Dao
interface AgentDao {
    @Query("SELECT * FROM AgentEntity")
    fun getAllAgents(): MutableList<AgentEntity>

    @Query("SELECT * FROM AgentEntity WHERE favourite")
    fun getFavoriteAgents(): MutableList<AgentEntity>

    @Insert
    fun addFavourite(agentEntity: AgentEntity)

    @Delete
    fun deleteFavourite(agentEntity: AgentEntity)
}