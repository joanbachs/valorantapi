package com.example.valorantapi.Entities

import androidx.room.Database
import androidx.room.RoomDatabase

@Database(entities = arrayOf(AgentEntity::class), version = 2)
abstract class AgentDatabase: RoomDatabase() {
    abstract fun agentDao(): AgentDao
}