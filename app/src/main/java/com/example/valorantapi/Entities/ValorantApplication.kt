package com.example.valorantapi.Entities

import android.app.Application
import androidx.room.Room
import androidx.room.RoomDatabase


class ValorantApplication: Application() {
    companion object{
        lateinit var database: AgentDatabase
    }

    override fun onCreate() {
        super.onCreate()
        database = Room.databaseBuilder(this,
            AgentDatabase::class.java,
            "AgentDatabase").build()
    }
}