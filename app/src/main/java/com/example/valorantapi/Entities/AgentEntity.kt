package com.example.valorantapi.Entities

import android.os.Parcelable
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "AgentEntity")
data class AgentEntity(
    @PrimaryKey(autoGenerate = true) var id: Long = 0,
    var name: String,
    var image: String,
    var description: String,
    var favourite: Boolean)