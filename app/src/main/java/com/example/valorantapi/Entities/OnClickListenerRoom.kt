package com.example.valorantapi.Entities

import com.example.valorantapi.AgentsAdapter
import com.example.valorantapi.agents.AgentData

interface OnClickListenerRoom {
    fun onClick(agent: AgentEntity)
    fun onClickDelete(agent: AgentEntity)
    fun onClickFavorite(agent: AgentEntity)
}