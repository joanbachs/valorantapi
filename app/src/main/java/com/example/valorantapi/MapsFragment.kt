package com.example.valorantapi

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.valorantapi.agents.AgentData
import com.example.valorantapi.maps.DataMaps
import com.example.valorantapi.maps.Maps
import com.example.valorantapi.weapons.DataWeapon

private lateinit var recyclerView: RecyclerView
private lateinit var viewModel: ValorantApiViewModel

class MapsFragment: Fragment(R.layout.maps_list_screen), OnClickListener {
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel = ViewModelProvider(this).get(ValorantApiViewModel::class.java)

        recyclerView = view.findViewById(R.id.recyclerview)

        viewModel.mapsData.observe(viewLifecycleOwner, Observer {
            setupRecyclerView(it)
        })
    }

    fun setupRecyclerView(maps: Maps) {
        recyclerView.layoutManager = LinearLayoutManager(context)
        recyclerView.adapter = MapsAdapter(maps.data, this)
    }

    override fun onClickAgent(agentData: AgentData) {
        TODO("Not yet implemented")
    }

    override fun onClickMap(dataMaps: DataMaps) {
        val action = MapsFragmentDirections.actionMapsFragmentToDetailFragment(dataMaps.displayName, dataMaps.displayIcon, dataMaps.coordinates)
        findNavController().navigate(action)
    }

    override fun onClickWeapon(dataWeapon: DataWeapon) {
        TODO("Not yet implemented")
    }
}