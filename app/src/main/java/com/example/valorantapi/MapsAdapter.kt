package com.example.valorantapi

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.valorantapi.agents.AgentData
import com.example.valorantapi.databinding.RecyclerviewItemBinding
import com.example.valorantapi.maps.DataMaps
import com.squareup.picasso.Picasso

class MapsAdapter(map: List<DataMaps>, private val listener: OnClickListener): RecyclerView.Adapter<MapsAdapter.ValorantApiViewHolder>() {
    // L'adapter agafarà la llista d'items que volem pinter i s'encarregarà de
    // gestionar tota interacció amb ella
    private val maps = map

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MapsAdapter.ValorantApiViewHolder {
        // Crea un layout recyclerview_item.xml i li passa a la classe ViewHolder
        val v = LayoutInflater
            .from(parent.context)
            .inflate(R.layout.recyclerview_item, parent, false)
        return ValorantApiViewHolder(v)
    }

    override fun onBindViewHolder(holder: MapsAdapter.ValorantApiViewHolder, position: Int) {
        // Cridarà al ViewHolder perquè pinti la informació d'un item
        holder.bindMapsData(maps[position])
    }

    // Tindrà el número d'elements de la llista de l'adapter
    override fun getItemCount(): Int = maps.size

    inner class ValorantApiViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
        val binding = RecyclerviewItemBinding.bind(itemView)

        fun setListener(mapData: DataMaps){
            binding.root.setOnClickListener {
                listener.onClickMap(mapData)
            }
        }

        fun bindMapsData(map: DataMaps) {
            setListener(map)
            Picasso.get().load(map.displayIcon).into(binding.snapImageview)
            binding.nameTextview.text = map.displayName
            binding.roleTextview.text = map.coordinates
        }
    }
}
