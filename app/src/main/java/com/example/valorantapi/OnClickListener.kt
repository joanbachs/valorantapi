package com.example.valorantapi

import com.example.valorantapi.agents.AgentData
import com.example.valorantapi.maps.DataMaps
import com.example.valorantapi.weapons.DataWeapon

interface OnClickListener {
    fun onClickAgent(agentData: AgentData)
    fun onClickMap(dataMaps: DataMaps)
    fun onClickWeapon(dataWeapon: DataWeapon)

}