package com.example.valorantapi

import android.os.Bundle
import android.text.Layout
import android.view.View
import android.widget.ImageButton
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.valorantapi.agents.AgentData
import com.example.valorantapi.agents.Data
import com.example.valorantapi.maps.DataMaps
import com.example.valorantapi.weapons.DataWeapon

private lateinit var recyclerView: RecyclerView
private lateinit var viewModel: ValorantApiViewModel
private lateinit var detailButton: ImageButton


class AgentsFragment: Fragment(R.layout.agents_list_screen), OnClickListener {

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel = ViewModelProvider(this).get(ValorantApiViewModel::class.java)

        recyclerView = view.findViewById(R.id.recyclerview)

        viewModel.agentsData.observe(viewLifecycleOwner, Observer {
            setupRecyclerView(it)
        })
    }

    fun setupRecyclerView(agentsData: Data) {
        recyclerView.layoutManager = LinearLayoutManager(context)
        recyclerView.adapter = AgentsAdapter(agentsData.data, this)
    }

    override fun onClickAgent(agentData: AgentData) {
        val action = AgentsFragmentDirections.actionAgentsFragmentToDetailFragment(agentData.displayName, agentData.displayIconSmall, agentData.description)
        findNavController().navigate(action)
    }

    override fun onClickMap(dataMaps: DataMaps) {
        TODO("Not yet implemented")
    }

    override fun onClickWeapon(dataWeapon: DataWeapon) {
        TODO("Not yet implemented")
    }
}