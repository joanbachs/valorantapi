package com.example.valorantapi

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.valorantapi.agents.AgentData
import com.example.valorantapi.maps.DataMaps
import com.example.valorantapi.maps.Maps
import com.example.valorantapi.weapons.DataWeapon
import com.example.valorantapi.weapons.Weapons

private lateinit var recyclerView: RecyclerView
private lateinit var viewModel: ValorantApiViewModel

class WeaponsFragment: Fragment(R.layout.weapons_list_screen), OnClickListener {
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel = ViewModelProvider(this).get(ValorantApiViewModel::class.java)

        recyclerView = view.findViewById(R.id.recyclerview)

        viewModel.weaponData.observe(viewLifecycleOwner, Observer {
            setupRecyclerView(it)
        })
    }

    fun setupRecyclerView(weapons: Weapons) {
        recyclerView.layoutManager = LinearLayoutManager(context)
        recyclerView.adapter = WeaponAdapter(weapons.data, this)
    }

    override fun onClickAgent(agentData: AgentData) {
        TODO("Not yet implemented")
    }

    override fun onClickMap(dataMaps: DataMaps) {
        TODO("Not yet implemented")
    }

    override fun onClickWeapon(dataWeapon: DataWeapon) {
        val action = WeaponsFragmentDirections.actionWeaponsFragmentToDetailFragment(dataWeapon.displayName, dataWeapon.displayIcon, dataWeapon.category.substring(21))
        findNavController().navigate(action)
    }
}