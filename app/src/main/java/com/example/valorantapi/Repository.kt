package com.example.valorantapi

import androidx.lifecycle.MutableLiveData
import com.example.valorantapi.agents.Data
import com.example.valorantapi.competitiveTiers.DataCT
import com.example.valorantapi.maps.DataMaps
import com.example.valorantapi.weapons.DataWeapon

class Repository {
    val apiInterface = ApiInterface.create()

    suspend fun getAgentsData(url: String) = apiInterface.getAgentsData(url)

    suspend fun getCTData(url: String) = apiInterface.getCompetitiveTiersData(url)

    suspend fun getMapsData(url: String)= apiInterface.getMapsData(url)

    suspend fun getWeaponsData(url: String)= apiInterface.getWeaponsData(url)
}