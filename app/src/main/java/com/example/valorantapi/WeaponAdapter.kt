package com.example.valorantapi

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.valorantapi.agents.AgentData
import com.example.valorantapi.databinding.RecyclerviewItemBinding
import com.example.valorantapi.weapons.DataWeapon
import com.example.valorantapi.weapons.Weapons
import com.squareup.picasso.Picasso

class WeaponAdapter(weapon: List<DataWeapon>, private val listener: OnClickListener): RecyclerView.Adapter<WeaponAdapter.ValorantApiViewHolder>() {
    // L'adapter agafarà la llista d'items que volem pinter i s'encarregarà de
    // gestionar tota interacció amb ella
    private val weapons = weapon

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): WeaponAdapter.ValorantApiViewHolder {
        // Crea un layout recyclerview_item.xml i li passa a la classe ViewHolder
        val v = LayoutInflater
            .from(parent.context)
            .inflate(R.layout.recyclerview_item, parent, false)
        return ValorantApiViewHolder(v)
    }

    override fun onBindViewHolder(holder: WeaponAdapter.ValorantApiViewHolder, position: Int) {
        // Cridarà al ViewHolder perquè pinti la informació d'un item
        holder.bindWeaponsData(weapons[position])
    }

    // Tindrà el número d'elements de la llista de l'adapter
    override fun getItemCount(): Int = weapons.size

    inner class ValorantApiViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
        val binding = RecyclerviewItemBinding.bind(itemView)

        fun setListener(dataWeapon: DataWeapon){
            binding.root.setOnClickListener {
                listener.onClickWeapon(dataWeapon)
            }
        }

        fun bindWeaponsData(weapon: DataWeapon) {
            setListener(weapon)
            Picasso.get().load(weapon.displayIcon).into(binding.snapImageview)
            binding.nameTextview.text = weapon.displayName
            binding.roleTextview.text = weapon.category.substring(21)
        }
    }
}
