package com.example.valorantapi

import com.example.valorantapi.agents.Data
import com.example.valorantapi.competitiveTiers.DataCT
import com.example.valorantapi.maps.DataMaps
import com.example.valorantapi.maps.Maps
import com.example.valorantapi.weapons.DataWeapon
import com.example.valorantapi.weapons.Weapons
import okhttp3.OkHttpClient
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Url

interface ApiInterface {
    @GET()
    suspend fun getAgentsData(@Url url: String): Response<Data>

    @GET()
    suspend fun getCompetitiveTiersData(@Url url: String): Response<DataCT>

    @GET()
    suspend fun getMapsData(@Url url: String): Response<Maps>

    @GET()
    suspend fun getWeaponsData(@Url url: String): Response<Weapons>

    companion object {
        val BASE_URL = "https://valorant-api.com/"
        fun create(): ApiInterface {
            val client = OkHttpClient.Builder().build()
            val retrofit = Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build()
            return retrofit.create(ApiInterface::class.java)
        }
    }
}
